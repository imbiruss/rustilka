# Налаштування та початок розробки.

Після того як ви створили заготовку за допомогою `cargo generate` ви тепер можете почати розробляти.
Як бачите, вже ініціалізований об'єкт `lilka`
```rust
let mut lilka = Lilka::new(Configuration::default()).unwrap();
```
Рядок `Configuration::default()` дає дефолтні значення, а саме:
- Частота чіпа 160MHz
- Частота SPI 60MHz
- Порядок оновлення "Vertical Refresh Order - Top To Bottom" і "Horizontal Refresh Order - Left To Right"
- Орієнтація екранцу в градусах 270. Міняти тільки тоді якщо знаєте що робите
- Тірінг дисплея вимкнений

Якщо хочете налаштувати під себе то можете це зробити через new():
```rust
//умовно у вас імпортовано prelude
//use rustilka::prelude::*;

let cfg = Configuration::new(50.MHz, ClockFreq::Clock80MHz, RefreshOrder::default(), Rotation::Deg270, TearingEffect::HorizontalAndVertical)
let lilka = Lilka::new(cfg).unwrap();
```

З самого початку вже ініціалізований embassy Executor, що дасть вам змогу писати async таски і усіляке інше, детальніше про async [тут](lib/lib_async.md).

Також вам не треба ініціалізувати peripherals та систему, все вже зроблено і знаходитья в об'єкту `lilka`
```rust
let rng_periph = lilka.peripherals.RNG; //це тільки об'єкт периферії для створення rng, як такий об'єкт він не буде мати функціональності
```

Якщо ж ви хочете використовувати всі можливості плати, то можете ~~активувати радості у всі штани~~ включити в `features` `full_fun`:
```toml
rustilka = {version = "0.0.1-alpha", features = ["full-fun"]}
```
Відтепер вам доступна вся периферія на платі, тому розважайтесь!