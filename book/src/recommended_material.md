# Є якісь приклади коду для rustilka/esp32s3?
Так звісно, їх звісно немає в монорепо rustilka, але все ж таки там можна розібрати як адаптувати цей код з бібліотекою.
Приклади [тут](https://github.com/esp-rs/esp-hal/tree/main/examples)

# Які бібліотеки можуть бути корисними чи в пригоді?
Наразі в [crates.io](https://crates.io) є навіть окремий каталог бібліотек для #[no_std] середовищ, що дуже добре для нашого випадку, але їх куча, складно знайти цікаві та необхідні, тому ця сторінка і для цього!

# embedded-graphics та різні додатки
Цей крейт стане вашим рендером, хоч і базова бібліотека має тільки примітивні лінії, фігури й різні прості малювання, але цього достатньо, для того що б зробити що завгодно. З іншими додатковими крейтами різноманіття збільшується.

[embedded-graphics](https://crates.io/crates/embedded-graphics)

[Список деяких корисних крейтів](https://github.com/embedded-graphics/embedded-graphics?tab=readme-ov-file#additional-functions-provided-by-external-crates)

[Драйвера для дисплеїв](https://github.com/embedded-graphics/embedded-graphics?tab=readme-ov-file#display-drivers)

# USB класи та різна функціональність.
Продовжуючи тему навчання embedded, то бібліотека не надає просте керування USB, дається тільки low-level доступ до периферії, і написати логіку повинні самотужки, наразі по підтримці класів USB все добре.

[usb-device](https://crates.io/crates/usb-device) головний дескриптор usb

[Дескриптор для HID(клавіатури, мишка і т.д.)](https://github.com/twitchyliquid64/usbd-hid)

[Інша реалізація HID](https://github.com/dlkj/usbd-human-interface-device)

[Serial usb](https://github.com/rust-embedded-community/usbd-serial)

[DFU](https://github.com/vitalyvb/usbd-dfu)

[USB-Ethernet](https://crates.io/crates/usbd-ethernet)

Switch_hal для зручної маніпуляції з Controller: [тут крейт](https://crates.io/crates/switch-hal).

