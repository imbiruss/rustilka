# Написання асинхронних задач за допомогою embassy executor

Коли ви згенерували проект за допомогою template, ви побачите свій стартовий код. 

І як ми бачимо тут вже ініціалізований executor
```rust
let syst = SystemTimer::new(lilka.peripherals.SYSTIMER); //Ініціалізація таймера для executor
rustilka::hal::embassy::init(&lilka.clock, syst); //Визначаємо цей таймер для роботи
let executor = EXECUTOR.init(embassy::executor::Executor::new());//Ініціалізуємо сам executor
```

Відтепер вже ви можете писати різні таски.

Давайте, наприклад напишемо два таски для перемикання світлодіодів асинхронно. 

По перше треба ініціалізувати піни для світлодіодів. 
```rust
let io = IO::new(lilka.peripherals.GPIO, lilka.peripherals.IO_MUX);//ініціалізуємо GPIO
let firstled = io.pins.gpio47.into_push_pull_output();//Перший 
let secondled = io.pins.gpio14.into_push_pull_output();//Другий
```

Оскільки у вас ініціалізований в Cargo.toml embassy_executor embassy_time, нам треба за допомогою макросів вказати таск.
```rust
#[embassy_executor::task]//обов'язково над функцією(також async обов'язково прописати)
async fn first_led(mut led: GpioPin<Output<PushPull>, 47>) {
    loop {
    embassy_time::Timer::after_millis(100u64).await;//чекаємо 100 мілісекунд
    led.set_high();//вмикаємо
    embassy_time::Timer::after_millis(100u64).await;
    led.set_low();//вимикаємо

    }
}
```

Далі напишемо інший аналогічний таск, але з іншим піном

```rust
#[embassy_executor::task]
async fn second_led(mut led: GpioPin<Output<PushPull>, 14>) {//зверніть увагу на інший номер піна в типі
    loop {
    embassy_time::Timer::after_millis(1000u64).await;//чекаємо секунду
    led.set_high();
    embassy_time::Timer::after_millis(1000u64).await;
    led.set_low();

    }
}
```

Таски готові, далі нам треба вже заспавнити таски для виконання, методом та внутрішнім замиканням ми вже зможемо їх спавнити
```rust
executor.run(|spawner| {//створюємо замикання і спавнимо в цьому блоці
        spawner.spawn(first_led(firstled)).expect("failed to run first led");//Спавнимо перший світлодіод
        spawner.spawn(second_led(secondled)).expect("failed to run second led");//Спавнимо другий світлодіод
    });
```