# Rustilka: Rust для Лілки

- [Мініпроєкт(не тільки бібліотека) Rustilka](./about_rustilka.md)
- [Що таке лілка?](./about_lilka.md)
- [Порівняння SDK і бібліотеки на Rust](./comparison.md)
- [Встановлення тулчейну та налаштування проекту](./creating_rom.md)
- [Про бібліотеку](./lib_main.md)
    - [Контроллер](./lib/lib_controller.md)
    - [Дисплей](./lib/lib_display.md)
    - [SD карта](./lib/lib_sd.md)
    - [UART](./lib/lib_serial.md)
    - [Батарейка](./lib/lib_battery.md)
    - [Зумер(не людина)](./lib/lib_buzzer.md)
    - [I2S](./lib/lib_i2s.md)
    - [Async програмування](./lib/lib_async.md)
- [Поширені запитання](./faq.md)
- [Корисні матеріали](./recommended_material.md)

