#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]


use rustilka::{hal::{
    clock::ClockControl,
    peripherals::Peripherals,
    embassy,
    Delay,
    },
    prelude::*
    };
use rustilka::{EXECUTOR, LilkaDisplay, State};

use esp_backtrace as _;

{% if log -%}
use esp_println::println;
{% endif %}

{% if wifi -%}
use esp_wifi::{initialize, EspWifiInitFor};
{% endif -%}


{% if alloc -%}
extern crate alloc;
use core::mem::MaybeUninit;
use rustilka::hal::{timer::TimerGroup, Rng};
#[global_allocator]
static ALLOCATOR: esp_alloc::EspHeap = esp_alloc::EspHeap::empty();

fn init_heap() {
    const HEAP_SIZE: usize = 32 * 1024;
    static mut HEAP: MaybeUninit<[u8; HEAP_SIZE]> = MaybeUninit::uninit();

    unsafe {
        ALLOCATOR.init(HEAP.as_mut_ptr() as *mut u8, HEAP_SIZE);
    }
}
{% endif -%}

#[entry]
fn main() - ! {


    {% if alloc -%}
    init_heap();
    {% endif -%}
    let mut lilka = Lilka::new(Clock160MHz).unwrap();
    //init embassy_executor
    let syst = SystemTimer::new(lilka.peripherals.SYSTIMER);
    rustilka::hal::embassy::init(&lilka.clock, syst);
    let executor = EXECUTOR.init(embassy::executor::Executor::new());
    {% if log -%}
    //init log
    esp_println::logger::init_logger_from_env();
    log::info!("Logger is setup");
    println!("Hello world!");
    {% endif -%}

    {% if wifi-%}
    //init wifi
    let timer = TimerGroup::new(peripherals.TIMG1, &clocks).timer0;
    let _init = initialize(
        EspWifiInitFor::Wifi,
        timer,
        Rng::new(peripherals.RNG),
        system.radio_clock_control,
        &clocks,
    )
    .unwrap();
    {% endif -%}
    loop {
    }
}
