#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_time::Timer;
use embedded_graphics::image::Image;
use embedded_graphics::mono_font::iso_8859_5::FONT_6X13_BOLD;
use embedded_graphics::mono_font::MonoTextStyle;
use esp_backtrace as _;
use embedded_graphics::pixelcolor::Rgb565;
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::Rectangle;
use embedded_graphics::text::Text;
use embedded_hal::digital::InputPin;
use embedded_text::alignment::{HorizontalAlignment, VerticalAlignment};
use embedded_text::style::{HeightMode, TextBoxStyleBuilder};
use embedded_text::TextBox;
use rustilka::hal::embassy;
use rustilka::ClockFreq::Clock160MHz;
use rustilka::hal::systimer::SystemTimer;
use rustilka::prelude::*;
use rustilka::{EXECUTOR, LilkaDisplay, State};
use tinybmp::Bmp;
#[entry]
fn main() -> ! {
    let mut lilka = Lilka::new(Clock160MHz).unwrap();
    lilka.display.clear(Rgb565::BLACK).unwrap();
    let syst = SystemTimer::new(lilka.peripherals.SYSTIMER);
    rustilka::hal::embassy::init(&lilka.clock, syst);
    let executor = EXECUTOR.init(embassy::executor::Executor::new());
    executor.run(|spawn| {
        spawn.spawn(run(lilka.state, lilka.display)).unwrap()
    });
    }
#[embassy_executor::task]
async fn run(mut controller: State, mut display: LilkaDisplay) {
    let start_img = include_bytes!("../rustilka_small.bmp");
    let start_img = Bmp::<Rgb565>::from_slice(start_img);
    let start_text = "Привіт, я rustilka\n\
    Це демонстраційна та експериментальна прошивка для тестування різної периферії та самого коду\n\
    A - Кіра_на_війні_шок.bmp картинка\n\
    B - Тест .gif\n\
    C - Прінт каталогу СД карти\n\
    D - Тест крестовини\n";

    let char_style = MonoTextStyle::new(&FONT_6X13_BOLD, Rgb565::WHITE);
    let textbox_style = TextBoxStyleBuilder::new()
        .height_mode(HeightMode::FitToText)
        .alignment(HorizontalAlignment::Center)
        .vertical_alignment(VerticalAlignment::Middle)
        .build();
    let bounds = Rectangle::new(Point::new(0, 130), Size::new(280, 0));
    let text_box = TextBox::with_textbox_style(start_text, bounds, char_style, textbox_style);

    'start: loop {
        Timer::after_millis(10u64).await;
        Image::new(&start_img.unwrap(), Point::new(0, 20)).draw(&mut display).unwrap();
        text_box.draw(&mut display).unwrap();
        if controller.a.is_low().unwrap() {
            println!("Press A");
            display.clear(Rgb565::BLACK).unwrap();
            let kira_data = include_bytes!("../kira_soldier.bmp");
            let kira_img = Bmp::<Rgb565>::from_slice(kira_data).unwrap();
            Image::new(&kira_img, Point::new(90, 30)).draw(&mut display).unwrap();
            let txt_a = Text::new("Шок, нема емоцій, одні слова\nДля повернення тицніть Start", Point::new(60, 200), MonoTextStyle::new(&FONT_6X13_BOLD, Rgb565::WHITE));
            txt_a.draw(&mut display).unwrap();
            loop {
                if controller.start.is_low().unwrap() {println!("Press Start");
                    display.clear(Rgb565::BLACK).unwrap();
                    continue 'start;}
            }
        }
    }


}
