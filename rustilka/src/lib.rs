#![no_std]

mod cfg;

pub extern crate esp_hal as hal;

use core::cell::RefCell;

use cfg_if::cfg_if;
use embassy_embedded_hal::shared_bus::blocking::spi::SpiDevice;
use embassy_sync::blocking_mutex::{raw::NoopRawMutex, NoopMutex};
use embedded_sdmmc::{Directory, SdCard, TimeSource, Timestamp, VolumeManager};
use hal::{analog::adc::{self}, analog::adc::{AdcConfig, AdcPin, Attenuation}, clock::ClockControl, clock::Clocks, delay::Delay, delay::Delay as ESPDelay, dma::DmaPriority, dma::{Channel0, Dma}, dma_buffers,  gpio,  gpio::{Analog, Output}, gpio::{GpioPin, Input}, i2s::{DataFormat, I2s, I2sTx, Standard}, peripherals::UART0, peripherals::{Peripherals, SPI2}, peripherals::{ADC1, I2S0}, prelude::*, spi::master::Spi, spi::{FullDuplexMode, SpiMode}, uart::config::{Config, DataBits, Parity, StopBits}, uart::TxRxPins, uart::Uart, Mode, Async, Blocking};
use mipidsi::{
    models::ST7789,
    options::{Orientation, Rotation},
    Builder, Display, NoResetPin,
};
use static_cell::StaticCell;
#[derive(Default)]
pub struct DummyTimesource();

impl TimeSource for DummyTimesource {
    fn get_timestamp(&self) -> Timestamp {
        Timestamp {
            year_since_1970: 0,
            zero_indexed_month: 0,
            zero_indexed_day: 0,
            hours: 0,
            minutes: 0,
            seconds: 0,
        }
    }
}
pub use hal::clock::CpuClock as ClockFreq;
use display_interface_spi::SPIInterface;
use embedded_hal::digital::OutputPin;
use hal::analog::adc::Adc;
use hal::gpio::{Io, Level, Pull};
use hal::gpio::Level::Low;
use hal::system::SystemControl;
use hal::uart::ClockSource;
use mipidsi::options::ColorInversion;
use crate::cfg::Configuration;

pub mod prelude {
    pub use crate::Lilka;
    pub use esp_println::*;
    pub use hal::prelude::*;
    pub use hal::clock::CpuClock as ClockFreq;
    pub use mipidsi::options::{RefreshOrder, Rotation, TearingEffect};
    pub use fugit::RateExtU32;
    pub use crate::cfg::Configuration;
}
pub static EXECUTOR: StaticCell<esp_hal_embassy::Executor> = StaticCell::new();
static SPI_BUS: StaticCell<NoopMutex<RefCell<Spi<'static, SPI2, FullDuplexMode>>>> =
    StaticCell::new();



#[cfg(feature = "battery")]
type AdcCal = adc::AdcCalCurve<ADC1>;

#[cfg(feature = "display")]
pub type LilkaDisplay = Display<
    SPIInterface<
        SpiDevice<
            'static,
            NoopRawMutex,
            Spi<'static, SPI2, FullDuplexMode>,
            Output<'static, GpioPin<7>>,
        >,
        Output<'static, GpioPin<15>>,
    >,
    ST7789,
    NoResetPin,
>;

#[cfg(feature = "sd")]
type SDC = SdCard<
    SpiDevice<
        'static,
        NoopRawMutex,
        Spi<'static, SPI2, FullDuplexMode>,
        Output<'static, GpioPin<16>>,
    >,
    Output<'static, GpioPin<28>>,
    ESPDelay,
>;

#[cfg(feature = "sd")]
type SDD = Directory;

#[cfg(feature = "sd")]
pub type SdVolMgr = VolumeManager<SDC, DummyTimesource>;
///System with the necessary configured peripherals.

pub struct Lilka {
    pub peripherals: Peripherals,
    pub clock: Clocks<'static>,
    pub delay: Delay,
    #[cfg(feature = "controller")]
    pub state: State,
    #[cfg(feature = "display")]
    pub display: LilkaDisplay,
    #[cfg(feature = "sd")]
    pub sd_vol_mgr: SdVolMgr,
    #[cfg(feature = "serial")]
    pub serial: Uart<'static, UART0, Async>,
    #[cfg(feature = "battery")]
    pub battery: Battery,
    #[cfg(feature = "buzzer")]
    pub buzzer: Buzzer,
    #[cfg(feature = "i2s")]
    pub i2s_tx: I2sTx<'static, I2S0, Channel0, Blocking>,
}

#[cfg(feature = "controller")]
///Collection of buttons for controlling.
///
///All buttons are pre-configured and you do not need to set them.
///```rust
///let mut lilka = Lilka::new(ClockFreq::Clock80MHz);
//    loop {
//        if lilka.state.a.is_active().unwrap() {
//            //do something
//        } else {
//            //do something else
//        }
//    }
/// ```
#[cfg(feature = "controller")]
pub struct State {
    pub up: Input<'static, GpioPin<38>>,
    pub down: Input<'static,GpioPin<41>>,
    pub left: Input<'static,GpioPin<39>>,
    pub right: Input<'static,GpioPin<40>>,
    pub a: Input<'static, GpioPin<5>>,
    pub b: Input<'static,GpioPin<6>>,
    pub c: Input<'static,GpioPin<10>>,
    pub d: Input<'static,GpioPin<9>>,
    pub select: Input<'static,GpioPin<0>>,
    pub start: Input<'static,GpioPin<4>>,
}

///Object for reading voltage of battery. Add to features if you want.
///
///If you need to read the battery charge, add "battery" to the features.
#[cfg(feature = "battery")]
pub struct Battery {
    adc: Adc<'static, ADC1>,
    adc_pin: AdcPin<GpioPin<3>, ADC1>,
}

#[cfg(feature = "buzzer")]
pub struct Buzzer(GpioPin<11>);

#[cfg(feature = "sd")]
pub struct SD;

impl Lilka {
    ///Start of fun :)
    /// ```rust
    /// let mut lilka = Lilka::new(Configuration::default()).unwrap();
    /// ```
    /// 
    pub fn new(config: Configuration) -> Result<Self, &'static str> {
        let peripherals = Peripherals::take();
        let system = SystemControl::new(peripherals.SYSTEM);
        let clocks = ClockControl::configure(system.clock_control, config.cpu_freq).freeze();
        let io = Io::new(peripherals.GPIO, peripherals.IO_MUX);
        let mut delay = Delay::new(&clocks);
        
        //Display
        cfg_if! {
            if #[cfg(feature = "display")] {
                Output::new(io.pins.gpio45, Level::High).set_high();
        let spimosi = io.pins.gpio17;
        let spimiso = io.pins.gpio8;
        let spiclk = io.pins.gpio18;
        let disp_dc = Output::new(io.pins.gpio15, Level::Low);
        let disp_cs = Output::new(io.pins.gpio7, Level::Low);
        let spi = Spi::new(peripherals.SPI2, config.spi_freq, SpiMode::Mode0, &clocks)
            .with_pins(
                Some(spiclk),
                Some(spimosi),
                Some(spimiso),
                gpio::NO_PIN,
            );
        let spi_bus = NoopMutex::new(RefCell::new(spi));
        let spi_bus = SPI_BUS.init(spi_bus);
        let disp_spi = SpiDevice::new(spi_bus, disp_cs);
        let di = SPIInterface::new(disp_spi, disp_dc);
        let display = match Builder::new(ST7789, di)
            .display_size(240, 280)
            .orientation(Orientation::new().rotate(config.disp_rotation))
                .display_offset(0, 20)
                .invert_colors(ColorInversion::Inverted)
                .refresh_order(config.disp_refresh_order)
            .init(&mut delay) {
                    Ok(d) => d,
                    Err(e) => return Err("Error creating display"),
                };
        }}

        cfg_if! {
            if #[cfg(feature = "controller")] {
        let up = Input::new(io.pins.gpio38, Pull::Up);
        let left = Input::new(io.pins.gpio39, Pull::Up);
        let right = Input::new(io.pins.gpio40, Pull::Up);
        let down = Input::new(io.pins.gpio41, Pull::Up);

        let a = Input::new(io.pins.gpio5, Pull::Up);
        let b = Input::new(io.pins.gpio6, Pull::Up);
        let c = Input::new(io.pins.gpio10, Pull::Up);
        let d = Input::new(io.pins.gpio9, Pull::Up);

        let start = Input::new(io.pins.gpio4, Pull::Up);
        let select = Input::new(io.pins.gpio0, Pull::Up);
        }}
        //Serial
        cfg_if! {
            if #[cfg(feature = "serial")] {
            let tx = io.pins.gpio43;
            let rx = io.pins.gpio44;
            let serial_pins = TxRxPins::new_tx_rx(
                tx,
                rx
            );
            let config = Config {
                baudrate: 115200,
                data_bits: DataBits::DataBits8,
                parity: Parity::ParityNone,
                stop_bits: StopBits::STOP1,
            clock_source: ClockSource::Apb,};
            let mut serial = Uart::new_async_with_config(peripherals.UART0, config, Some(serial_pins), &clocks );
        }}
        //SD
        cfg_if! {
        if #[cfg(feature = "sd")] {
                let sd_cs = Output::new(io.pins.gpio16, Level::Low);
                let sd_spi = SpiDevice::new(spi_bus, sd_cs);
                let sd = SdCard::new(sd_spi, Output::new(io.pins.gpio28, Level::Low), delay);
                let mut vol_mgr = VolumeManager::new(sd, DummyTimesource::default());
            }}
        //Battery
        cfg_if! {
        if #[cfg(feature = "battery")] {
            let mut adc1_config = AdcConfig::new();
            let pin = adc1_config.enable_pin_with_cal::<_, ()>(
            io.pins.gpio3,
            Attenuation::Attenuation11dB,
        );
            let adc1 = Adc::new(peripherals.ADC1, adc1_config);
        }}

        //I2S
        cfg_if! {
            if #[cfg(feature = "i2s")] {
                let dma = Dma::new(peripherals.DMA);
                let dma_channel = dma.channel0;
                    let (tx_buffer, mut tx_descriptors, _, mut rx_descriptors) = dma_buffers!(32000, 0);
                let bclk = io.pins.gpio42;
                let dout = io.pins.gpio2;
                let lrck = io.pins.gpio1;
                let i2s = I2s::new(
                    peripherals.I2S0,
                    Standard::Philips,
                    DataFormat::Data16Channel16,
                    44100u32.Hz(),
                    dma_channel.configure(
                        false,
                        &mut tx_descriptors,
                        &mut rx_descriptors,
                        DmaPriority::Priority0,
                    ),
                    &clocks
                );
                let i2s_tx = i2s.i2s_tx.with_bclk(bclk).with_dout(dout).build();
            }
        }
        Ok(Lilka {
            peripherals: unsafe { Peripherals::steal() },
            clock: clocks,
            delay,
            #[cfg(feature = "controller")]
            state: State {
                up,
                down,
                left,
                right,
                a,
                b,
                c,
                d,
                select,
                start,
            },
            #[cfg(feature = "display")]
            display,
            #[cfg(feature = "sd")]
            sd_vol_mgr: vol_mgr,
            #[cfg(feature = "serial")]
            serial,
            #[cfg(feature = "battery")]
            battery: Battery::new(adc1, pin),
            #[cfg(feature = "buzzer")]
            buzzer: Buzzer(io.pins.gpio11),
            #[cfg(feature = "i2s")]
            i2s_tx,
        })
    }
}
#[cfg(feature = "battery")]
impl Battery {
    #[doc(hidden)]
    fn new(adc: Adc<'static, ADC1>, adc_pin: AdcPin<GpioPin<3>, ADC1>) -> Self {
        Battery { adc, adc_pin }
    }
    ///
    /// read u16 voltage in milivolts
    ///
    pub fn read_voltage(&mut self) -> u16 {
        unimplemented!()
    }
}
