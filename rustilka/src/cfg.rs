use fugit::RateExtU32;
use mipidsi::options::{RefreshOrder, Rotation, TearingEffect};
use crate::ClockFreq;
pub struct Configuration {
    pub cpu_freq: ClockFreq,
    pub spi_freq: fugit::HertzU32,
    pub disp_refresh_order: RefreshOrder,
    pub disp_rotation: Rotation,
    pub disp_tearing: TearingEffect,
}
/// Default configuration
impl Default for Configuration {    
    fn default() -> Configuration {
        Configuration {
            spi_freq: 60.MHz(),
            cpu_freq: ClockFreq::Clock160MHz,
            disp_refresh_order: RefreshOrder::default(),
            disp_rotation: Rotation::Deg270,
            disp_tearing: TearingEffect::Off,
        }
    }
}
impl Configuration {
    ///For creating configuration
    pub fn new(spi_freq: fugit::HertzU32, cpu_freq: ClockFreq, disp_refresh_order: RefreshOrder, disp_rotation: Rotation, disp_tearing: TearingEffect) -> Configuration {
        Configuration {
            spi_freq,
            cpu_freq,
            disp_refresh_order,
            disp_rotation,
            disp_tearing,
        }
    }
    
}
