[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
# Rustilka: Rust on [Lilka](https://github.com/and3rson/lilka/tree/main)

# Currently WIP!!! The project is not completely ready, it is undergoing a beta test.


### Support library AKA BSP for Lilka board based on ESP32-S3. 
To use it, you must install toolchain via espup: https://esp-rs.github.io/book/installation/riscv-and-xtensa.html

The BSP periphery has almost all the peripherals that can be optional, but by default `display`, `controller`, `serial`, `sd` are included
If you want to enable all the peripherals, enable like this:
```toml
rustilka = {version = "0.0.1-alpha", features = ["full-fun"]}
```
At the moment, the library is not perfect and not ready for easy use, so some of the peripherals that have already been implemented may change, here is the status of peripheral support:
- [x] Display.
- [x] Controller.
- [ ] SdCard `Not tested`
- [x] onboard UART header.
- [x] battery reader. Partially, the calculation with resistors is not implemented yet.
- [x] Buzzer `PushPull output is selected for the user to his implementation.`
- [x] Support for I2S onboard modules. TX-only. `Not tested`

Read more about `configuration` [in this section](https://rust.lilka.dev/lib_main.html).


In order not to suffer from project configuration, you can generate a project using `cargo generate`.
```sh
cargo install cargo-generate

cargo generate https://gitlab.com/imbiruss/rustilka.git template
```
