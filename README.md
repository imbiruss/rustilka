[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Book - WIP](https://img.shields.io/badge/Book-WIP-FFA500?logo=mdbook)](https://rust.lilka.dev)
# Rustilka: Rust on Lilka
![](assets/rustilka_logo.png)
>Для лого не було ідей і творчості...
### Бібліотека для проєкту [Лілка](https://github.com/and3rson/lilka) на мові Rust
Бібліотека яка полегшує створення проєктів, завдяки необхідній конфігурацій периферії.

Код ніяк не залежить від оригінального SDK і немає біндів (тільки Pure Rust), тому можуть бути можливі помилки чи гівнокод.
Основні нюанси це використання [esp-hal](https://github.com/esp-rs/esp-hal) і проєкту [embassy](https://embassy.dev/) які пере-експортовані в бібліотеці для користування.

Для більш детального дослідження нюансів, цілей, питань та як створити почати розробку є [мікрокнига](https://rust.lilka.dev/)

Особливості:
+ Переваги мови Rust не прибираються, безпека понад усе і будь де(цікавий факт, в коді бібліотеки є трохи unsafe:) ).


+ Опціональна підтримка async функціоналу для швидкодії. `Для цього бажано встановити nightly`


+ Як казалось раніше, повністю кастомна реалізація.

## Статус розробки та сама бібліотека [тут](https://gitlab.com/imbiruss/rustilka/-/tree/master/rustilka?ref_type=heads).

## Створення проєкту
На зараз з нуля налаштувати проєкт хоч і можна, але це доволі муторно, адже для використання embassy та різної async функціональності треба багато чого налаштувати, тому для цього був створений генератор базований на [cargo-generate](https://github.com/cargo-generate/cargo-generate).

```
cargo generate https://gitlab.com/imbiruss/rustilka.git template
```
Для більшого уточнення параметрів при генеруванні прочитайте [цю сторінку книги](https://rust.lilka.dev/creating_rom.html)

Наразі сам hal на якому побудована бібліотека по суті вже застарілий, бо нещодавно став уніфікованим, але через деякі проблеми які не дають білдити бінарники буде використовуваться перед останній коміт де ще не угробили окремі hal.
https://github.com/esp-rs/esp-hal
